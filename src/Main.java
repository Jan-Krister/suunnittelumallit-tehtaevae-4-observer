import java.util.Scanner;

public class Main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		DigiKello digi = new DigiKello();
		Ajastin ajastin = new Ajastin();
		Thread säie = new Thread(ajastin);

		ajastin.addObserver(digi);
		Scanner scanner = new Scanner(System.in);
		System.out.println("Aloita painamalla nappia");
		scanner.nextLine();
		säie.start();

		System.out.println("Lopeta nappia painamalla");
		scanner.nextLine();
		ajastin.pysäytä();
		scanner.close();
	}

}
