import java.util.Observable;

public class Ajastin extends Observable implements Runnable {

	
	boolean käynnissä = true;
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		int laskuri = 0;
		while(käynnissä) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			if(laskuri == 60) {
				laskuri = 0;
			}
			laskuri++;
			setChanged();
			notifyObservers(laskuri);
		}
	}
	
	public void pysäytä() {
		käynnissä = false;
	}
}
