import java.util.Observable;
import java.util.Observer;

public class DigiKello implements Observer {
	private int tunti;
	private int minuutti;
	private int sekunti;

	@Override
	public void update(Observable o, Object aika) {
		sekunti = (int) aika;
		if (sekunti == 60) {
			minuutti++;
		}
		if (minuutti == 60) {
			tunti++;
			minuutti = 0;
		}
		System.out.println(tunti + " : " + minuutti + " : " + sekunti);

	}

}
